#include <ArduinoJson.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <OneWire.h> 
#include <Arduino.h>
#include <WEMOS_SHT3X.h>
#include <ArduinoJson.h>
#include <DallasTemperature.h>
#define SECRET
//SERIAL>>>>>>><<<<
#include <Wire.h>
#define RXD2 16
#define TXD2 17
 
//variables distancia y control de altura
const int trigPin = 2;
const int echoPin = 5;
float distancia = 0;
int nivel = 0; //nivel en porciento
long duration = 0;
float distance = 0;
float distanciaPromedio = 0;
//bomba
  int relay = 23;
  
const int numReadings = 20;
double readings[numReadings];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
double total = 0;                  // the running total
double average = 0;
#define DIST_TOPE 11
//>>>>>>>>>>>>>>>>>>>>>>>>>BOMBA ESTADO
bool estadobomba = false;
//>>>>>>>>>>>>>>>>>>>>>>>>>>variables temperatura 
OneWire ourWire(4); //Se establece el pin 4 del ESP32 para la lectura del sensor
DallasTemperature DS18B20(&ourWire); //Se declara una variable u objeto para el sensor
//>>>>>>>>>>>>>>>>>>>>>>>>>>variables de pH
#include "DFRobot_ESP_PH.h"
#include <EEPROM.h>
DFRobot_ESP_PH ph;
#define ESPADC 4096.0   //the esp Analog Digital Convertion value
#define ESPVOLTAGE 3300 //the esp voltage supply value
#define PH_PIN 35  //the esp gpio data pin number
float voltage, phValue;
float valotemp;
//Variables del Wifi
const char* ssid = "Parditos_2.4Gnormal";
const char* password = "R4f4pandit4";
//variable de la Base de datos
const char* serverName ="https://us-east-1.aws.webhooks.mongodb-realm.com/api/client/v2.0/app/application-0sensor-xelnk/service/APIITSA/incoming_webhook/api?secret=itsa123";
//variable de enviado.
StaticJsonDocument<500> doc;
int ultrasonic_fail = 0;//contador para determinar fallo del sensor ultrasonico


float reading = 0;
/*
WEB SERVER
*/
WiFiServer server(80);
String header;
String output23State = "off";
const int output23 = 23;


unsigned long currentTime = millis();
unsigned long previousTime = 0; 
const long timeoutTime = 2000;


void setup()
{
Serial.begin(9600); //Se inicia el monitor serie a 115200 baudios

//bomba
  pinMode(output23, OUTPUT);
  digitalWrite(output23, LOW);
  //temperatura
  Wire.begin();
  //proceso setup de temperatura 
DS18B20.begin(); //Se inicia el sensor

//proceso setup de distancia
pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
pinMode(echoPin, INPUT); // Sets the echoPin as an Input
//proceso setup de pH
 ph.begin();
//proceso setup de wifi
Serial.print("conectando ");
Serial.print(ssid);
Serial.print("con contraseña");
Serial.println(password);
WiFi.begin(ssid,password);
while(WiFi.status()!=WL_CONNECTED){
  delay(500);
  Serial.print(".");
  }
while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
  
}

float calculopH(){
  float lecturapH=0;
   static unsigned long timepoint = millis();
 if (millis() - timepoint > 1000U) //time interval: 1s
 {
  timepoint = millis();
  //voltage = rawPinValue / esp32ADC * esp32Vin
  voltage = analogRead(PH_PIN) / ESPADC * ESPVOLTAGE; // read the voltage
  Serial.print("voltage:");
  Serial.println(voltage, 4);
  valotemp= DS18B20.getTempCByIndex(0);
   Serial.print("temperature:");
  Serial.print(valotemp, 1);
  Serial.println("^C");
  phValue = ph.readPH(voltage, valotemp); // convert voltage to pH with temperature compensation
  Serial.print("pH:");
  Serial.println(phValue, 4);
 }
 ph.calibration(voltage, valotemp); // calibration process by Serail CMD
 lecturapH= phValue;
doc["pH"] = lecturapH;
      return lecturapH;
  
}
double filtroProm(double entrada) {
  // subtract the last reading:
  total = total - readings[readIndex];
  // read from the sensor:
  readings[readIndex] = entrada;
  // add the reading to the total:
  total = total + readings[readIndex];
  // advance to the next position in the array:
  readIndex++;
 
  // if we're at the end of the array...
  if (readIndex >= numReadings) {
    // ...wrap around to the beginning:
    readIndex = 0;
  }
 
  // calculate the average:
  average = total / numReadings;
  return average;
}
float get_dist() {
  float lecturaDistancia=0;
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
 
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
 
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH, 200000);//timeout de 200000 microsegundos que es 200ms
 
  // Calculating the distance
  distance = (float)(duration * 0.03432 )/ 2;
  Serial.println("Distancia: ");
  Serial.println(distance);

 // return (float)(duration * 0.03432 ) / 2;
 lecturaDistancia=distance;
   doc["Distancia"] = lecturaDistancia;
      return lecturaDistancia;
  
}
 
float get_level() {
  distance = get_dist();
  float lecturanivel = 0;
  if (distance == 0)ultrasonic_fail++; //se incrementa el contador
  if (ultrasonic_fail == 5) { //si llega a 5 veces concecutivas con lecturas a cero es que algo anda mal con el sensor
  while (1) {
    }
  }
 
  if (distance > 0) {//descarta errores del sensor
    ultrasonic_fail = 0; //se resetea el contador
    distanciaPromedio = filtroProm(distance);
    Serial.print(distance); Serial.print(" ; ");
    Serial.println(distanciaPromedio);
    nivel = DIST_TOPE - distanciaPromedio;
    nivel = map(nivel, 0, DIST_TOPE - 2.7, 0, 10);//9 seria el nivel maximo por seguridad que del sensor (30cm)
    Serial.println("Nivel de Agua XD");
    Serial.println(nivel);
    lecturanivel= nivel;

  }
  doc["Nivel"] = lecturanivel;
      return lecturanivel;
  
}
//BOMBA
#define CTROL_RELAY_GPIO 23
boolean llenando = true;
int bombaFail_counter = 0;
//OJO ESTOS NIVELES ESTAN EN PORCIENTO

bool aviso = true;
bool pump_on = false;
 
void control_Pump(unsigned int ControlPin) {
 

   Serial.write('1');
//    pump_on = true;
  
 
}
//metodo para arduino Key studio esclavo
void requestEvent()
{
  Wire.requestFrom(2, 17);    // Le pide 17 bytes al Esclavo 2
  while(Wire.available())    // slave may send less than requested
  { 
    char c = Wire.read();   // Recibe byte a byte
    Serial.print(c);        // Presenta los caracteres en el Serial Monitor
  }
    Serial.println();       // Cámbia de línea en el Serial Monitor.
    delay(500);
  Wire.requestFrom(3, 17);    // Le pide 17 bytes al Esclavo
  while(Wire.available())
  { 
    char c = Wire.read();
    Serial.print(c); 
  }                  
}

void POSTData()
{
      
      if(WiFi.status()== WL_CONNECTED){
      HTTPClient http;

      http.begin(serverName);
      http.addHeader("Content-Type", "application/json");

      String json;
      serializeJson(doc, json);

      Serial.println(json);
      int httpResponseCode = http.POST(json);
      Serial.println(httpResponseCode);
      }
}
void getDevice()
{

    esp_sleep_wakeup_cause_t wakeup_reason;
    wakeup_reason = esp_sleep_get_wakeup_cause();

    uint64_t chipid=ESP.getEfuseMac();//The chip ID is essentially its MAC address(length: 6 bytes).
    Serial.printf("***ESP32 Chip ID = %04X%08X\n",(uint16_t)(chipid>>32),(uint32_t)chipid);//print High 2 bytes
    char buffer[200];
    sprintf(buffer, "%04X%08X",(uint16_t)(chipid>>32),(uint32_t)chipid);
    
    doc["IP"] = WiFi.localIP().toString();
    doc["RSSI"] = String(WiFi.RSSI());
}
float sensortemp(){
DS18B20.requestTemperatures(); //Se envía el comando para leer la temperatura

reading = 0;

if (DS18B20.getTempCByIndex(0)){
  reading= DS18B20.getTempCByIndex(0); //Se obtiene la temperatura en ºC

}

    
    doc["Sensor_temperatura"] = reading;
      return reading;
  
  }

void websserver(){
  WiFiClient client = server.available();   // Listen for incoming clients

  if (client) {                             // If a new client connects,
    currentTime = millis();
    previousTime = currentTime;
    Serial.println("New Client.");          // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected() && currentTime - previousTime <= timeoutTime) {  // loop while the client's connected
      currentTime = millis();
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
            // turns the GPIOs on and off
            if (header.indexOf("GET /23/on") >= 0) {
              Serial.println("GPIO 23 on");
              output23State = "on";
              digitalWrite(output23, HIGH);
            } 
            else if (header.indexOf("GET /23/off") >= 0) {
              Serial.println("GPIO 23 off");
              output23State = "off";
              digitalWrite(output23, LOW);
            }
            // Display the HTML web page
            client.println("<!DOCTYPE html><html>");

            client.println("<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' integrity='sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u' crossorigin='anonymous'>");
            client.println("<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js' integrity='sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa' crossorigin='anonymous'></script>");
            client.println("<link rel='stylesheet' href='https://goo.gl/wRTxw5'><link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'><link href='https://fonts.googleapis.com/icon?family=Material+Icons' rel='stylesheet'>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            //client.println("<link rel=\"icon\" href=\"data:,\">");
            client.println("<link rel='stylesheet' href='https://goo.gl/wRTxw5'><link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'><link href='https://fonts.googleapis.com/icon?family=Material+Icons' rel='stylesheet'>");

            // CSS to style the on/off buttons 
            // Feel free to change the background-color and font-size attributes to fit your preferences
            client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
            client.println(".button { background-color: #1e7fd4; border: none; color: white; padding: 16px 40px;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".button2 {background-color: #870b0b;}</style></head>");
            
            // Web Page Heading
            client.println("<body><h1>ESP32 Web Server ITSA Atlixco</h1>");
            client.println("<body><h4>Sistemas Programables</h4>");
            client.println("<div class='container-fluid'>");
              /*
              Logica para el estado de la bomba.
             */
           valotemp= DS18B20.getTempCByIndex(0);  
           phValue =ph.readPH(voltage, valotemp);
            String estado ;
            if (output23State =="off"){
             estado = "Encendida";
              }
             else{
             estado = "Apagada";
             }
             /*
              Logica para letreo de aceptable.
             */
             String estadoagua;
             String aguaicon;
             if((valotemp >=8.8 || valotemp <25) && (phValue >= 7 || phValue < 8.7)){
              estadoagua =" Buen estado para consumo humano";
              aguaicon="bueno";
              }
             if((valotemp ==3.7 || valotemp <8.8) && (phValue <6)){
              estadoagua =" Estado regular para superviencia";
              aguaicon="regular";
              }
             if((valotemp >25 || valotemp <140) && (phValue > 8.8)){
              estadoagua =" Estado de peligo";
              aguaicon="peligroso";
              }
             if((valotemp ==-127 || valotemp <-140) && (phValue ==-3 || phValue < 0)){
              estadoagua =" Estado de peligo";
              aguaicon="peligroso";
              }
          client.println("<p>Estado actual de la Bomba " + estado + "</p>");
          client.println("<table class='table table-striped table-dark'><thead>");
          client.println("<tr class='bg-primary'><h2><i class='fa fa-thermometer'></i>Temperatura: "+String(valotemp)+"</h2></tr>");
          client.println("<tr class='bg-success'><h2><i class='glyphicon glyphicon-scale'></i>Distancia: "+String(distance)+"</h2></tr>");
          client.println("<tr class='bg-warning'><h2><i class='glyphicon glyphicon-tint'></i>pH: "+String(phValue)+"</h2></tr>");
          client.println("<tr class='bg-info'><h5>"+estadoagua+"</h5></tr>");
          if (aguaicon =="bueno"){
            client.println("<tr class='bg-warning'><h2><i class='glyphicon glyphicon-ok-signt'></i></h2></tr>");
            }
          if (aguaicon =="regular"){
            client.println("<tr class='bg-warning'><h2><i class='glyphicon glyphicon-remove-sign'></i></h2></tr>");
            }
          if (aguaicon =="peligroso"){
            client.println("<tr class='bg-warning'><h2><i class='glyphicon glyphicon-alert'></i></h2></tr>");
            }       
           client.println("</thead><tbody></table>");
           
            // If the output26State is off, it displays the ON button       
            if (output23State=="off") {
              client.println("<p><a href=\"/23/on\"><button class=\"button\">OFF</button></a></p>");
            } else {
              client.println("<p><a href=\"/23/off\"><button class=\"button button2\">ON</button></a></p>");
            } 
            
            client.println("</div>");
            client.println("</body></html>");
            
            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } 
           else if (c != '\r') {  // if you got anything else but a carriage return character,
           currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
    }
 }
  
void loop()
{
  websserver();
  getDevice();
  sensortemp();
  get_dist();
  get_level();
  calculopH();
  requestEvent();

  Serial.println("valores enviados desde ESP32");
  Serial.println("Posting...");
   POSTData();
   serializeJsonPretty(doc, Serial);
   Serial.println("\nDone.");



   delay(1000);
}
