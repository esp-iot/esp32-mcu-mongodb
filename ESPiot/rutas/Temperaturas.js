const express = require('express');
const router = express.Router();

const Temperatura = require('../models/temperatura')

router.get('/', async (req, res) => {
    try {
        const arrayTemperaturas= await Temperatura.find();
        console.log(arrayTemperaturas)
        res.render("temperaturas", {
            arrayTemperaturas
        })
    } catch (error) {
        console.log(error)
    }
})

module.exports = router;
