const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const temperaturaSchema = new Schema({
    IP:String,
    RSSI:String,
    Temperatura:String,
	ts:Date
});

// Crear el modelo
const Temperatura = mongoose.model('Temperatura', temperaturaSchema);

module.exports = Temperatura;
